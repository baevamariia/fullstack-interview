/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $ProductView = {
    properties: {
        name: {
            type: 'string',
            isRequired: true,
        },
        unit_cost: {
            type: 'number',
            isRequired: true,
        },
        unit_revenue: {
            type: 'number',
            isRequired: true,
        },
        id: {
            type: 'number',
            isRequired: true,
        },
    },
} as const;
