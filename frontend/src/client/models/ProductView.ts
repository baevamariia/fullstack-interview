/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type ProductView = {
    name: string;
    unit_cost: number;
    unit_revenue: number;
    id: number;
};

